#include "stdafx.h"
#include <iostream>
#include <conio.h>
#include <string>
#include "Pokémon.h"
#include "Location.h"

using namespace std;

vector<Pokemon*>Pokemons;
void decisionPhase();
void begin(int& x);

void begin(int& x)
{
	string name;
	int slots = 0;
	int starterNo;
	Pokemons.resize(slots);

	cout << "Hey there trainer! Professor Oak here!! What's your name?" << endl;
	cin >> name;
	cout << endl;

	system("cls");

	cout << "Well hello there, " << name << "!" << endl;
	cout << "Welcome to your journey with Pokemons!" << endl << "I have three Pokemons here, and I currently have one to offer you." << endl << "Which starter Pokemon will you choose?" << endl;
	cout << "1. Treecko" << endl << "2. Torchic" << endl << "3. Mudkip" << endl;
	cin >> starterNo;

	x = starterNo;
	if (starterNo == 1)
	{
		cout << "You have picked Treecko as your companion!" << endl;
		Pokemons[0]->Pokemons("Treecko", 100, 15, 1, 0);
	}
	if (starterNo == 2)
	{
		cout << "You have picked Torchic as your companion!" << endl;
		Pokemons[0]->Pokemons("Torchic", 100, 8, 1, 0);
	}
	if (starterNo == 3)
	{
		cout << "You have picked Mudkip as your companion!" << endl;
		Pokemons[0]->Pokemons("Mudkip", 100, 10, 1, 0);
	}
	else
	{
		cout << "Sorry, you need a Pokemon Companion to pass through." << endl;
		begin(x);
	}
	decisionPhase();
}

void addPokemon(int& x, int& y, int& slots)
{
	y = rand() % 4 + 1;
	if (x == 1 || y == 1)
	{
		Pokemons[slots]->Pokemons("Treecko", 100, 15, 1, 0);
	}
	if (x == 2 || y == 2)
	{
		Pokemons[slots]->Pokemons("Torchic", 100, 8, 1, 0);
	}
	if (x == 3 || y == 3)
	{
		Pokemons[slots]->Pokemons("Mudkip", 100, 10, 1, 0);
	}

	x = 0;
	slots++;
}

void battlePhase()
{
	Pokemon* enemyPokemon = new Pokemon("Makuhita", 20, 2, NULL, NULL);
	Pokemons[0]->battle(enemyPokemon);
}


void decisionPhase()
{
	int n, x, y, slots;
	cout << "Choose what to do" << endl << "1. Battle" << endl << "2. Run away" << endl << "3. Catch" << endl;
	cin >> n;
	switch (n)
	{
	case 1:
		cout << "Battle commence!" << endl;
		if (n == 1)
		{
			battlePhase();
		}
		break;
	case 2:
		cout << "You ran away successfully." << endl;
		break;
	case 3:
		cout << "You caught him!!" << endl;
		addPokemon(x, y, slots);
		break;
	default:
		cout << "Invalid input. Try again!" << endl;
		decisionPhase();
		break;
	}
}

int main()
{
	int x;
	begin(x);
	while (1)
	{
		void decisionPhase();
		movement();
	}
	return 0;
}
