#include "stdafx.h"
#include <string>
#include <iostream>
#include "Pok�mon.h"

Pokemon::Pokemon(string name, int hp, int damage, int exp, int level)
{
	this->name = name;
	this->HP = HP;
	this->damage = damage;
	this->exp = exp;
}

vector<Pokemon*> Pokemon::Pokemons(string name, int hp, int damage, int level, int exp)
{
	this->name = name;
	this->HP = HP;
	this->damage = damage;
	this->exp = exp;
	return vector<Pokemon*>();
}

void Pokemon::battle(Pokemon* enemyPokemon)
{
	system("CLS");
	cout << "You are battling " << enemyPokemon->name << endl;
	cout << "You brought out your " << this->name << endl;

	while (1)
	{
		if (enemyPokemon->HP >= 0)
		{
			cout << this->name << " attacked " << enemyPokemon->name << endl;
			enemyPokemon->HP -= this->damage;
			cout << enemyPokemon->name << " remaining hp is " << enemyPokemon->HP << endl;
		}
		system("pause");
	}
	levelUp();
}

void Pokemon::levelUp()
{
	this->exp = exp + (exp * 0.2);
	if (exp >= expNextLevel)
	{
		this->level = level + 1;
		this->HP = HP + (HP * 0.15);
		this->damage = damage + (damage * 0.1);
		this->expNextLevel = expNextLevel + (expNextLevel * 0.1);
		this->exp = 0;
	}
}
