#pragma once
#include "stdafx.h"
#include <string>
#include <iostream>
#include <conio.h>
#include <vector>

using namespace std;

class Pokemon
{
public:
	vector<Pokemon*>Pokemons(string name, int hp, int damage, int level, int exp);
	Pokemon(string name, int hp, int damage, int level, int exp);
	string name = "";
	int baseHP = 100;
	int damage = 0;
	int HP = 0;
	int exp = 0;
	double expNextLevel = 10;
	int level = 1;
	void battle(Pokemon* enemyPokemon);
	void levelUp();

}Treecko, Torchic, Mudkip;